# Generated by Django 3.2.9 on 2021-11-15 18:33

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auth_example', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='user',
            new_name='name',
        ),
    ]
