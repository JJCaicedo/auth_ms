# Generated by Django 3.2.9 on 2021-11-15 20:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth_example', '0002_rename_user_user_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='las_change_date',
            field=models.DateTimeField(null=True),
        ),
    ]
