from .userCreateView    import UserCreateView
from .userDetailView    import UserDetailView, UserDeleteView, UserUpdateView
from .verifyToken       import VerifyTokenView